/*
 * Copyright sablintolya@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.ma1uta.matrix.client.api;

import static io.github.ma1uta.matrix.client.api.AdminApi.PATH;

import io.github.ma1uta.matrix.Secured;
import io.github.ma1uta.matrix.client.model.admin.AdminResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 * Gets information about a particular user.
 * <p/>
 * <a href="https://matrix.org/docs/spec/client_server/r0.3.0.html#id112">Specification.</a>
 */
@Api(value = PATH, description = "Gets information about a particular user")
@Path(PATH)
@Produces(MediaType.APPLICATION_JSON)
public interface AdminApi {

    /**
     * Admin api url.
     */
    String PATH = "/_matrix/client/r0/admin";

    /**
     * This API may be restricted to only be called by the user being looked up, or by a server admin. Server-local administrator
     * privileges are not specified in this document.
     *
     * @param userId          Required. The user to look up.
     * @param servletRequest  servlet request.
     * @param servletResponse servlet response.
     * @param securityContext security context.
     * @return Status code 200: The lookup was successful.
     */
    @ApiOperation(value = "his API may be restricted to only be called by the user being looked up, or by a server admin. "
        + "Server-local administrator privileges are not specified in this document.", response = AdminResponse.class)
    @ApiResponses( {
        @ApiResponse(code = 200, message = "The lookup was successful.")
    })
    @GET
    @Secured
    @Path("/whois/{userId}")
    AdminResponse whois(@ApiParam(value = "The use to look up", required = true) @PathParam("userId") String userId,
                        @Context HttpServletRequest servletRequest, @Context HttpServletResponse servletResponse,
                        @Context SecurityContext securityContext);
}
