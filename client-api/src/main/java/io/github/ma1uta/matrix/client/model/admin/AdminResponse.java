/*
 * Copyright sablintolya@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.ma1uta.matrix.client.model.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Map;

/**
 * Response for gets information about a particular user.
 */
@ApiModel(description = "Response for gets information about a particular user.")
public class AdminResponse {

    /**
     * The Matrix user ID of the user.
     */
    @ApiModelProperty(name = "user_id", value = "he Matrix user ID of the user.")
    @JsonProperty("user_id")
    private String userId;

    /**
     * Each key is an identitfier for one of the user's devices.
     */
    @ApiModelProperty("Each key is an identitfier for one of the user's devices")
    private Map<String, DeviceInfo> devices;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Map<String, DeviceInfo> getDevices() {
        return devices;
    }

    public void setDevices(Map<String, DeviceInfo> devices) {
        this.devices = devices;
    }
}
